import 'package:flutter/material.dart';
import 'package:interview_project/di/injection.dart';
import 'package:interview_project/ui/details/details_screen.dart';
import 'package:interview_project/ui/search/search_screen.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await configureDependencies();

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Interview App',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      routes: {
        "/": (context) => const SearchScreen(),
        "/details": (context) => const DetailsScreen(),
      },
    );
  }
}
