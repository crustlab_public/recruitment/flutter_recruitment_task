import 'package:injectable/injectable.dart';
import 'package:interview_project/network/app_service.dart';
import 'package:interview_project/repository/book.dart';

@injectable
class BooksRepository {
  final AppService appService;

  BooksRepository(this.appService);

  Future<List<Book>> getBooks() async {
    var booksDto = await appService.getBooks();
    return booksDto.books.map((book) => Book.fromDto(book)).toList();
  }
}
