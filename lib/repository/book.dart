import 'package:interview_project/network/model/books_dto.dart';

class Book {
  final VolumeInfo volumeInfo;

  Book({required this.volumeInfo});

  factory Book.fromDto(BookDto dto) {
    return Book(volumeInfo: VolumeInfo.fromDto(dto.volumeInfo));
  }
}

class VolumeInfo {
  final String title;
  final List<String> authors;
  final String? publisher;
  final String description;

  VolumeInfo(
      {required this.title,
      required this.authors,
      this.publisher,
      required this.description});

  factory VolumeInfo.fromDto(VolumeInfoDto dto) {
    return VolumeInfo(
      title: dto.title,
      authors: dto.authors,
      publisher: dto.publisher,
      description: dto.description,
    );
  }
}
