import 'package:dio/dio.dart';
import 'package:interview_project/network/model/books_dto.dart';
import 'package:retrofit/retrofit.dart';

part 'app_service.g.dart';

@RestApi()
abstract class AppService {
  factory AppService(Dio dio) = _AppService;

  @GET("books/v1/volumes?q=harry+potter")
  Future<BooksDto> getBooks();
}
