import 'package:json_annotation/json_annotation.dart';

part 'books_dto.g.dart';

@JsonSerializable()
class BooksDto {

  @JsonKey(name: "items")
  final List<BookDto> books;

  BooksDto(this.books);

  factory BooksDto.fromJson(
      Map<String, dynamic> json,
      ) =>
      _$BooksDtoFromJson(json);
}

@JsonSerializable()
class BookDto {
  @JsonKey(name: "volumeInfo")
  final VolumeInfoDto volumeInfo;

  BookDto(this.volumeInfo);

  factory BookDto.fromJson(
      Map<String, dynamic> json,
      ) =>
      _$BookDtoFromJson(json);
}

@JsonSerializable()
class VolumeInfoDto {
  @JsonKey(name: "title")
  final String title;
  @JsonKey(name: "authors")
  final List<String> authors;
  @JsonKey(name: "publisher")
  final String? publisher;
  @JsonKey(name: "description")
  final String description;

  VolumeInfoDto(this.title, this.authors, this.publisher, this.description);

  factory VolumeInfoDto.fromJson(
      Map<String, dynamic> json,
      ) =>
      _$VolumeInfoDtoFromJson(json);
}
