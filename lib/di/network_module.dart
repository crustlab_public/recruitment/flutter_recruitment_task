import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:injectable/injectable.dart';
import 'package:interview_project/network/app_service.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

@module
abstract class NetworkModule {
  static const String _baseUrl = "https://www.googleapis.com/";

  @singleton
  Dio getDio(
  ) {
    final dio = Dio(BaseOptions(baseUrl: _baseUrl));

    if (!kReleaseMode) {
      dio.interceptors.add(PrettyDioLogger(responseHeader: true));
    }

    return dio;
  }

  AppService getAppService(Dio dio) => AppService(dio);
}
