import 'package:get_it/get_it.dart';
import 'package:injectable/injectable.dart';
import 'package:interview_project/di/injection.config.dart';

final locator = GetIt.instance;

@injectableInit
Future<void> configureDependencies() async {
  await locator.init();
}
